import { ADD_PRODUCT, ADD_QUANTITY, SUBTRACT_QUANTITY } from './types';
import product from '../interfaces/cart.interface'
import State from '../interfaces/State.interface'

const addProduct = (product: product, state: State) => {
    let counter: number = 0;
    if (state.products.length > 0) {
        for (let i of state.products) {
            if (i.id === product.id) {
                i.quantity += product.quantity;
                counter++;
                console.log(state)
                return state
            }
        }
    }
    if (counter === 0) {
        const newProduct = [...state.products, product];
        console.log(newProduct)
        return {
            ...state,
            products: newProduct
        }
    }
}

const addQuantity = (id: string, state: State) => {
    let State = state.products.map((currPro: product) =>
        currPro.id === id ? { ...currPro, quantity: currPro.quantity + 1 } : currPro
    )
    console.log(State)
    return {
        ...state,
        products: State
    }

}

const subtractQuantity = (id: string, state: State) => {
    let State = state.products.map((currPro: product) => {
        console.log(currPro.id, id)
        if (currPro.id === id) {
            let currentPro = { ...currPro, quantity: currPro.quantity - 1 };
            if (currentPro.quantity > 0) {
                return currentPro
            }
        } else {
            return currPro
        }
    })
    console.log(State);
    let returnArr: product[] = [];
    if (State.length > 0) {
        for (let prod of State){
            if (prod !== undefined){
            returnArr.push(prod) 
            }
        };
            return {
            ...state,
            products: returnArr
        }
    } else {
        return {
            ...state,
            products: []
        }
    }

};

interface ACTION  {
    type: string,
    payload: string | product
}

export default (state:State, action: ACTION) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return addProduct(action.payload as product, state);
        case ADD_QUANTITY:
            return addQuantity(action.payload as string, state);
        case SUBTRACT_QUANTITY:
            return subtractQuantity(action.payload as string, state);
        default:
            return state;
    }
}