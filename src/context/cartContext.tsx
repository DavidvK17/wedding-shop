import {createContext} from 'react'
import CartCon from '../interfaces/CartCon.interface'

const CartContext = createContext<CartCon>({
    products: [],
    addProduct: (product) => {},
    addQuantity: (id) => {},
    subtractQuantity: (id) => {},
})

export default CartContext