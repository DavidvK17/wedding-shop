export default interface StateforLBW{
    photoIndex: number,
    isOpen: boolean,
    images: string[],
}