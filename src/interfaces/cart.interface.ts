export default interface product {
    id: string,
    title: string,
    price: number,
    imageSrc: string,
    quantity: number,
    variantID: string,
}