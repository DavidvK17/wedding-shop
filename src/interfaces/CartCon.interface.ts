import product from './cart.interface'

export default interface CartCon{
    products: product[],
    addQuantity: (id: string) =>void,
    addProduct: (product: product) => void,
    subtractQuantity: (id: string) => void,
}