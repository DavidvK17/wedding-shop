import product from './cart.interface'

export default interface State { products: product[] }