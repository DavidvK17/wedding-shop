import React from "react";
import { useQuery } from "react-apollo-hooks";
import gql from "graphql-tag";
import { ProductsData, ProductsDataVariables } from "./generated/ProductsData";
import { ImageContentType } from "./generated/globalTypes";
import "antd/dist/antd.css";
import { Row, Col, Card, Button } from "antd";
import { Link } from 'react-router-dom';

export const PRODUCTS_QUERY = gql`
query ProductsData($preferredContentType: ImageContentType) {
  products(first: 25) {
    edges {
      node {
        id
        title
        description
        priceRange {
          maxVariantPrice {
            amount
          }
          minVariantPrice {
            currencyCode
          }
        }
        updatedAt
        ...ProductImages
      }
    }
  }
}

fragment ProductImages on Product {
  images(first: 1) {
    edges {
      node {
        id
        transformedSrc(maxWidth: 1500, maxHeight: 1000, preferredContentType: $preferredContentType)
        altText
      }
    }
  }
}
`;

export default function Products() {
  const { data, loading } = useQuery<ProductsData, ProductsDataVariables>(
    PRODUCTS_QUERY, {
    variables: { preferredContentType: ImageContentType.JPG },
    suspend: false
  });

  if (loading || !data) {
    return <div>Loading products...</div>;
  }
  return (
    <div style={{ backgroundColor: "lightgrey" }}>
      <Row gutter={24} align="top" justify="center">
        <Col span={2}></Col>
        <Col span={20}>
          <Row align="top" justify="space-around">
            {data.products.edges.map(({ node: product }) => (
              <Link to={{
                pathname: `/details/${product.id}`
              }}>
                <Card
                  hoverable
                  style={{ marginTop: 15, marginBottom: 15, }}>
                  <div key={product.id}>
                    <h2>{product.title}</h2>
                    <p style={{ maxWidth: 400, minHeight: 75 }}>{product.description}</p>
                    <div>
                      <div className="image-item">
                        <img
                          style={{ maxWidth: 400, maxHeight: '400' }}
                          src={product.images.edges[0].node.transformedSrc}
                          alt={product.title} />
                      </div>
                    </div>
                    <Button type="primary" style={{ marginTop: "15px" }}>
                      <Link to={{
                        pathname: `/details/${product.id}`
                      }}>Details</Link>
                    </Button>
                  </div>
                </Card>
              </Link>
            ))}
          </Row>
        </Col>
        <Col span={2}></Col>
      </Row>
    </div>
  );
}