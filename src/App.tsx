import React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import createClient from "./apolloClient";
import Products from "./Products";
import Details from './components/Details'
import Impressum from './components/Impressum'
import Datenschutz from './components/Datenschutz'
import "./App.css";
import {Switch, Route} from 'react-router-dom';
import Header from './components/Header'
import Footer from './components/Footer'
import { useReducer } from "react";
import CartContext from './context/cartContext'
import cartReducer from './context/CartReducer'
import {ADD_PRODUCT, ADD_QUANTITY, SUBTRACT_QUANTITY} from './context/types'
import product  from './interfaces/cart.interface' 
import State from './interfaces/State.interface'


const client = createClient();

export default function App() {

  const initialStateADD_PRODUCT: State = {products: []
  }

  
  
  
  const [state, dispach] = useReducer(cartReducer, initialStateADD_PRODUCT);
  
  const addProduct = (product: product) => {
    dispach({
      type: ADD_PRODUCT,
      payload: product
    })
  }

  const addQuantity = (id: string) => {
    dispach({
      type: ADD_QUANTITY,
      payload: id
    })
  }

  const subtractQuantity = (id: string) => {
    dispach({
      type: SUBTRACT_QUANTITY,
      payload: id
    })
  }

  console.log(state.products)

  return (
    <CartContext.Provider
    value = {{
      products: state.products,
      addProduct,
      addQuantity,
      subtractQuantity,
    }}>
    <ApolloProvider client={client}>
      <ApolloHooksProvider client={client}>
        <Header />
        <Switch>
    <Route exact path="/" component={Products} />
    <Route exact path="/details/:id" component={Details} />
    <Route exact path="/impressum" component={Impressum} />
    <Route exact path="/datenschutz" component={Datenschutz} />
        </Switch>
      <Footer/>
      </ApolloHooksProvider>
    </ApolloProvider>
    </CartContext.Provider>
  );
}