import { InMemoryCache } from "apollo-cache-inmemory"; // data stored in datastore for apollo, no need for extra graph QL API request if asking for the same information, pulls it from cache instead, all stored in memory
import { ApolloClient } from "apollo-client";   // actual client that controls commmunication flow between React frontend and server
import { ApolloLink } from "apollo-link";   // chains together number of function calls, so you can halt the request or pull data from cache
import { setContext } from "apollo-link-context"; // so that github knows what our auhorization token is
import { HttpLink } from "apollo-link-http";    // terminating link, last link that eventually goes out and performs the graphQL API request
import { onError } from "apollo-link-error"; // if error happens, you can capture it
import produce from "immer";

const errorLink = onError(({ graphQLErrors, networkError, operation }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Path: ${path}`)
    );
  }

  if (networkError) {
    console.log(
      `[Network error ${operation.operationName}]: ${networkError.message}`
    );
  }
});

const createClient = () => {
  const cache = new InMemoryCache();

  const httpLink = new HttpLink({
    uri: `https://wedding-app.myshopify.com/api/graphql.json`
  });

  const authLink = setContext((_, oldContext) => {
    return produce(oldContext, draft => {
      if (!draft.headers) {
        draft.headers = {};
      }
      draft.headers[
        "X-Shopify-Storefront-Access-Token"
      ] = `9aa9068cadb1d0f8a3bd2bce153997b9`;
    });
  });

  const client = new ApolloClient({
    cache,
    link: ApolloLink.from([errorLink, authLink, httpLink])
  });

  return client;
};

export default createClient;