type ShopifyURL = string;
type ShopifyDateTime = string;
type ShopifyDecimal = number;
type ShopifyMoney = number;