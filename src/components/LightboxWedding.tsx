import React, { Component } from 'react';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; 
import { Button, Tooltip } from 'antd';
import StateforLBW from '../interfaces/LBWState.interface'
import PropsforLBW from '../interfaces/LBWProps.interface'


export default class LightboxWedding extends Component<PropsforLBW, StateforLBW> {
  constructor(props: PropsforLBW) {
    super(props);
 
    this.state = {
      photoIndex: 0,
      isOpen: false,
      images:this.props.imgArray
    };
  }
 
  render() {
    const { photoIndex, isOpen, images } = this.state;
    
    return (
      <div>
        <div>
        <Tooltip title="Fotos in voller Größe anzeigen lassen." placement="right">
        <Button onClick={() => this.setState({ isOpen: true })} type="primary">
         Fotos anzeigen
        </Button>
        </Tooltip>
        </div>
        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
          />
        )}
      </div>
    );
  }
}