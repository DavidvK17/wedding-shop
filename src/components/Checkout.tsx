import React, { useContext } from 'react';
import gql from 'graphql-tag';
import { useMutation } from "react-apollo-hooks";
import { CheckoutProducts, CheckoutProducts_checkoutCreate } from "../generated/CheckoutProducts";
import CartContext from '../context/cartContext'
import "antd/dist/antd.css";
import { Button } from "antd";
import { CheckoutCreateInput } from '../generated/globalTypes'

export const CHECKOUT_QUERY = gql`
mutation CheckoutProducts($input: CheckoutCreateInput!) {
  checkoutCreate(input: $input) {
    userErrors {
      message
      field
    }
    checkout {
      id
      webUrl
      subtotalPrice
      totalTax
      totalPrice
      lineItems (first:250) {
        pageInfo {
          hasNextPage
          hasPreviousPage
        }
        edges {
          node {
            title
            variant {
              title
              image {
                src
              }
              price
            }
            quantity
          }
        }
      }
    }
  }
}

`;


export default function CheckoutFunction() {
  const [checkoutProduct] = useMutation<CheckoutProducts, { input: CheckoutCreateInput }>(CHECKOUT_QUERY);

  const context = useContext(CartContext)
  const getArray = () => context.products.map((product) => {
    return { variantId: product.variantID, quantity: product.quantity }
  })


  

  const submitCheckout = async () => {
    const input: CheckoutCreateInput = {
      lineItems: getArray(),
    };
    const checkout= await checkoutProduct({ variables: { input: input } });
    window.open(checkout.data.checkoutCreate.checkout.webUrl);
    
  }


  return (
    <div>
      <Button type="primary" onClick={submitCheckout} disabled={context.products.length!==0 ? false : true}>
        Checkout
      </Button>


    </div>
  );
}