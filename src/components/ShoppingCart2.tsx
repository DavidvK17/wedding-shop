import React, {useContext, useState, ChangeEvent, FormEvent} from 'react'
import {PRODUCTS_QUERY} from '../Products'
import { useQuery } from 'react-apollo-hooks';
import { ProductsData, ProductsDataVariables, ProductsData_products_edges } from '../generated/ProductsData';
import CartContext from '../context/cartContext';
import product from '../interfaces/cart.interface';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { notification } from 'antd';

interface PROPS {
    id: string,
    variantID: string
}

function ShoppingCart2 (props: PROPS) {

const {data, loading} = useQuery<ProductsData, ProductsDataVariables>(PRODUCTS_QUERY);

const [quantity, setQuantity] = useState(
    1
);

const context = useContext(CartContext)

let myProduct: ProductsData_products_edges;
let Cart: product;

const addToCart = function (ev: FormEvent<HTMLFormElement>){
    ev.preventDefault();
    let rightID : string = myProduct.node.id;
    
    Cart = {variantID: props.variantID,  id: rightID, title: myProduct.node.title, quantity, price: Number(myProduct.node.priceRange.maxVariantPrice.amount), imageSrc: myProduct.node.images.edges[0].node.transformedSrc};
    context.addProduct(Cart);
    setQuantity(1)
}

const openNotification = (placement:any) => {
    notification.info({
      message: 'Produkt wurde in den Warenkorb hinzugefügt!',
      description:
        'Du hast gerade eines unserer tollen Produkte in deinen Warenkorb hinzugefügt. Mach den nächsten Schritt im Warenkorb!',
         placement, icon: <ShoppingCartOutlined style={{ color: '#108ee9'}} />,
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  };

if(loading){
    return (
        <div>loading...</div>
    )
}
for(let i of data.products.edges){
    if(props.id === i.node.id){
            myProduct = i
        }
    };

return (
    <div className='containerHzf'>
    <form onSubmit = {addToCart}>
        <label>Anzahl:&nbsp;
    <input className='hzfInput'type = 'number' value = {quantity} onChange = {(ev: ChangeEvent<HTMLInputElement>, ) : void => setQuantity(Number(ev.target.value))}></input>
        </label>
        <button className='hzf' type = 'submit' disabled = {quantity <= 0 ? true : false} style = {quantity <=0 ? {backgroundColor: 'grey'} : {backgroundColor: 'dodgerblue'}} onClick={() => openNotification('bottomRight')} >Hinzufügen</button>
    </form>
</div>
)
}
export default ShoppingCart2