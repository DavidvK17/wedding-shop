import React, { Component } from 'react';
import { Layout, } from 'antd';
import { Link } from 'react-router-dom';

const { Footer } = Layout;


class FooterShop extends Component<any> {
    render() {
        return (
            <div className='footer'>
                <div className='footer-right'>
                    <span >Academy/Academic Work Germany ©2020 Created by David, Alex and Saliha</span>
                </div>
                <div className='footer-left'>
                    <Link to={{
                        pathname: `/impressum`
                    }}>Impressum</Link> |
              <Link to={{
                        pathname: `/datenschutz`
                    }}>Datenschutz</Link>
                </div>
            </div>


        )
    }
}

export default FooterShop