import React from 'react';
import '../App.css';
import { Card, Row, Col } from 'antd';
import LightboxWedding from '../components/LightboxWedding';
import { useQuery } from "react-apollo-hooks";
import gql from "graphql-tag";
import { DetailsData, DetailsDataVariables } from "../generated/DetailsData";
import { ImageContentType } from "../generated/globalTypes";
import { RouteComponentProps } from "react-router-dom";
import ShoppingCart2 from './ShoppingCart2'

export const DETAILS_QUERY = gql`
query DetailsData($preferredContentType: ImageContentType) {
  products(first: 25) {
    edges {
      node {
        id
        title
        description
        variants(first: 1) {
          edges {
            node {
              id
            }
          }
        }
        priceRange {
          maxVariantPrice {
            amount
          }
          minVariantPrice {
            currencyCode
          }
        }
        updatedAt
        ...ProductImages
      }
    }
  }
}

fragment ProductImages on Product {
  images(first: 10) {
    edges {
      node {
        id
        transformedSrc(maxWidth: 1500, maxHeight: 1000, 
          preferredContentType: $preferredContentType)
        altText
      }
    }
  }
}
`;

type TParams = { id: string };

export default function Details ({ match }: RouteComponentProps<TParams>) {

  const { data, loading } = useQuery<DetailsData, DetailsDataVariables>(
    DETAILS_QUERY, {
    variables: { preferredContentType: ImageContentType.JPG },
    suspend: false
  });

  if (loading || !data) {
    return <div>Produkt wird geladen...</div>;
  }

  const data_id_Arr = data.products.edges.map(({ node: product }) => product.id);
  const found = data_id_Arr.findIndex( element => element === match.params.id );
  const product = data.products.edges[ found ].node;
  const variantID = product.variants.edges[0].node.id
 
  const images_for_LightBox = 
  product.images.edges.map(({ node: productImages }) => productImages.transformedSrc);
  
  const images_for_Detail = 
  product.images.edges.map(({ node: productImages }) => 
  <img style={{ width: 300, padding: 10 }} src={productImages.transformedSrc} />)



  return (

    <div className="site-card-border-less-wrapper">
      <div className="site-drawer-render-in-current-wrapper">
        <Row>
          <Col span={2}></Col>
          <Col span={20}>
            <Card title={<span>{product.title}</span>} bordered={false} >
              {<p >{product.description}</p>}
              <Row justify="center" align="top">
                <LightboxWedding imgArray={images_for_LightBox} />
              </Row >
              <br/>
              <Row justify="center" align="top">
                {images_for_Detail}
                </Row>
              <Row justify="center" align="top">
                Einzelpreis: {product.priceRange.maxVariantPrice.amount}€
              </Row >
              <Row justify="center" >
                <ShoppingCart2 
        id = {product.id}
        variantID = {variantID} /></Row>
            </Card>
          </Col>
          <Col span={2}></Col>
        </Row>
      </div>
    </div>
  )

};
