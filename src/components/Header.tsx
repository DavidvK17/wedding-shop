import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { HomeOutlined } from '@ant-design/icons';
import ShoppingCartIcon2 from './ShoppingCartIcon2'


class HeaderShop extends Component<any> {
    render() {
        return (

            <div className='header'>

                <div> 
                <span className='home'>
                    <Link to="/" className="hometext">
                       <HomeOutlined /> Home
                    </Link>
                    </span>
                </div>

                <div className="atelier">
                    <a  href='https://ateliergarosa.mypixieset.com/' target='__blank'>Atelier Garossa</a>
                    <br/>
                    <span>Nachhaltige Papeterie</span>
                </div>

                <div>
                <span className='home'>
                    <ShoppingCartIcon2 />
                    </span>
                </div>

            </div>


        )
    }
}

export default withRouter(HeaderShop)