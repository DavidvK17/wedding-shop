import React, { useContext, useState, MouseEvent } from 'react'
import CartContext from '../context/cartContext'
import { Drawer, Badge, Row, Col, Button } from 'antd'
import { ShoppingCartOutlined, PlusCircleFilled, MinusCircleFilled } from '@ant-design/icons'
import './ShoppingCart.css'
import CheckoutFunction from '../components/Checkout'
import product from '../interfaces/cart.interface'

function ShoppingCartIcon2() {
    const context = useContext(CartContext);

    const [visible, setVisible] = useState(
        false
    );



    const onClose = () => {
        setVisible(false)
    };
    const showDrawer = () => {
        setVisible(true)
    }


    return (
        <div>
            <Drawer
                title="Your Order"
                placement="right"
                closable={false}
                onClose={onClose}
                visible={visible}
                getContainer={false}
                width={350}
                style={{ position: 'fixed' }}
            >
                {context.products.length === 1 && <Row style={{ color: "black" }}>
                    <Col span={12}>
                        <p >{context.products[0].title}</p>
                    </Col>
                    <Col span={12}>
                        <p>Preis: {context.products[0].price * context.products[0].quantity},00€</p>
                    </Col>
                    <Col span={12}>
                        <p>Anzahl: {context.products[0].quantity}</p>

                        <Button type='primary' value={context.products[0].id} 
                        onClick={(ev: MouseEvent<HTMLButtonElement>) => { ev.preventDefault(); 
                        context.addQuantity(ev.currentTarget.value) }} icon={<PlusCircleFilled />}
                        >
                        </Button>

                        <Button type='primary' value={context.products[0].id} 
                        onClick={(ev: MouseEvent<HTMLButtonElement>) => 
                        { ev.preventDefault(); context.subtractQuantity(ev.currentTarget.value) }} 
                        icon={<MinusCircleFilled />}
                        >
                        </Button>
                    </Col>
                    <Col span={12}>
                        <img src={context.products[0].imageSrc} className='smallImg'></img>
                    </Col>
                </Row>}

                {context.products.length > 1 && <div>
                    {context.products.map((product: product) => {
                        return (
                            <Row style={{ color: "black" }}>
                                <Col span={12}>
                                    <p >{product.title}</p>
                                </Col>
                                <Col span={12}>
                                    <p>Preis: {product.price * product.quantity},00€</p>
                                </Col>
                                <Col span={12}>
                                    <p>Anzahl: {product.quantity}</p>
                                    
                                    <Button type='primary' value={product.id} 
                                    onClick={(ev: MouseEvent<HTMLButtonElement>) => 
                                    { ev.preventDefault(); context.addQuantity(ev.currentTarget.value) }} 
                                    icon={<PlusCircleFilled />}
                                    >
                                    </Button>

                                    <Button type='primary' value={product.id} 
                                    onClick={(ev: MouseEvent<HTMLButtonElement>) => 
                                    { ev.preventDefault(); context.subtractQuantity(ev.currentTarget.value) }} 
                                    icon={<MinusCircleFilled />}>
                                    </Button>
                                </Col>
                                <Col span={12}>
                                    <img src={product.imageSrc} className='smallImg'></img>
                                </Col>
                            </Row>
                        )
                    })}
                </div>}
                <div style={{  position: 'fixed', bottom: '30px'}}>
                <CheckoutFunction />
                </div>
            </Drawer>
            <Badge count={context.products.length}>
                < ShoppingCartOutlined onClick={showDrawer} className='cartIcon' />
            </Badge>

        </div>
    )
}

export default ShoppingCartIcon2