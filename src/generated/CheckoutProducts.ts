/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CheckoutCreateInput } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: CheckoutProducts
// ====================================================

export interface CheckoutProducts_checkoutCreate_userErrors {
  __typename: "UserError";
  /**
   * The error message.
   */
  message: string;
  /**
   * Path to the input field which caused the error.
   */
  field: string[] | null;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems_pageInfo {
  __typename: "PageInfo";
  /**
   * Indicates if there are more pages to fetch.
   */
  hasNextPage: boolean;
  /**
   * Indicates if there are any pages prior to the current page.
   */
  hasPreviousPage: boolean;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node_variant_image {
  __typename: "Image";
  /**
   * The location of the image as a URL.
   */
  src: ShopifyURL;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node_variant {
  __typename: "ProductVariant";
  /**
   * The product variant’s title.
   */
  title: string;
  /**
   * Image associated with the product variant. This field falls back to the product image if no image is available.
   */
  image: CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node_variant_image | null;
  /**
   * The product variant’s price.
   */
  price: ShopifyMoney;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node {
  __typename: "CheckoutLineItem";
  /**
   * Title of the line item. Defaults to the product's title.
   */
  title: string;
  /**
   * Product variant of the line item.
   */
  variant: CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node_variant | null;
  /**
   * The quantity of the line item.
   */
  quantity: number;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems_edges {
  __typename: "CheckoutLineItemEdge";
  /**
   * The item at the end of CheckoutLineItemEdge.
   */
  node: CheckoutProducts_checkoutCreate_checkout_lineItems_edges_node;
}

export interface CheckoutProducts_checkoutCreate_checkout_lineItems {
  __typename: "CheckoutLineItemConnection";
  /**
   * Information to aid in pagination.
   */
  pageInfo: CheckoutProducts_checkoutCreate_checkout_lineItems_pageInfo;
  /**
   * A list of edges.
   */
  edges: CheckoutProducts_checkoutCreate_checkout_lineItems_edges[];
}

export interface CheckoutProducts_checkoutCreate_checkout {
  __typename: "Checkout";
  /**
   * Globally unique identifier.
   */
  id: string;
  /**
   * The url pointing to the checkout accessible from the web.
   */
  webUrl: ShopifyURL;
  /**
   * Price of the checkout before shipping and taxes.
   */
  subtotalPrice: ShopifyMoney;
  /**
   * The sum of all the taxes applied to the line items and shipping lines in the checkout.
   */
  totalTax: ShopifyMoney;
  /**
   * The sum of all the prices of all the items in the checkout, taxes and discounts included.
   */
  totalPrice: ShopifyMoney;
  /**
   * A list of line item objects, each one containing information about an item in the checkout.
   */
  lineItems: CheckoutProducts_checkoutCreate_checkout_lineItems;
}

export interface CheckoutProducts_checkoutCreate {
  __typename: "CheckoutCreatePayload";
  /**
   * List of errors that occurred executing the mutation.
   */
  userErrors: CheckoutProducts_checkoutCreate_userErrors[];
  /**
   * The new checkout object.
   */
  checkout: CheckoutProducts_checkoutCreate_checkout | null;
}

export interface CheckoutProducts {
  /**
   * Creates a new checkout.
   */
  checkoutCreate: CheckoutProducts_checkoutCreate | null;
}

export interface CheckoutProductsVariables {
  input: CheckoutCreateInput;
}
