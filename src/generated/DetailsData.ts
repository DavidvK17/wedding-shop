/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ImageContentType, CurrencyCode } from "./globalTypes";

// ====================================================
// GraphQL query operation: DetailsData
// ====================================================

export interface DetailsData_products_edges_node_variants_edges_node {
  __typename: "ProductVariant";
  /**
   * Globally unique identifier.
   */
  id: string;
}

export interface DetailsData_products_edges_node_variants_edges {
  __typename: "ProductVariantEdge";
  /**
   * The item at the end of ProductVariantEdge.
   */
  node: DetailsData_products_edges_node_variants_edges_node;
}

export interface DetailsData_products_edges_node_variants {
  __typename: "ProductVariantConnection";
  /**
   * A list of edges.
   */
  edges: DetailsData_products_edges_node_variants_edges[];
}

export interface DetailsData_products_edges_node_priceRange_maxVariantPrice {
  __typename: "MoneyV2";
  /**
   * Decimal money amount.
   */
  amount: ShopifyDecimal;
}

export interface DetailsData_products_edges_node_priceRange_minVariantPrice {
  __typename: "MoneyV2";
  /**
   * Currency of the money.
   */
  currencyCode: CurrencyCode;
}

export interface DetailsData_products_edges_node_priceRange {
  __typename: "ProductPriceRange";
  /**
   * The highest variant's price.
   */
  maxVariantPrice: DetailsData_products_edges_node_priceRange_maxVariantPrice;
  /**
   * The lowest variant's price.
   */
  minVariantPrice: DetailsData_products_edges_node_priceRange_minVariantPrice;
}

export interface DetailsData_products_edges_node_images_edges_node {
  __typename: "Image";
  /**
   * A unique identifier for the image.
   */
  id: string | null;
  /**
   * The location of the transformed image as a URL.
   * 
   * All transformation arguments are considered "best-effort". If they can be applied to an image, they will be.
   * Otherwise any transformations which an image type does not support will be ignored.
   */
  transformedSrc: ShopifyURL;
  /**
   * A word or phrase to share the nature or contents of an image.
   */
  altText: string | null;
}

export interface DetailsData_products_edges_node_images_edges {
  __typename: "ImageEdge";
  /**
   * The item at the end of ImageEdge.
   */
  node: DetailsData_products_edges_node_images_edges_node;
}

export interface DetailsData_products_edges_node_images {
  __typename: "ImageConnection";
  /**
   * A list of edges.
   */
  edges: DetailsData_products_edges_node_images_edges[];
}

export interface DetailsData_products_edges_node {
  __typename: "Product";
  /**
   * Globally unique identifier.
   */
  id: string;
  /**
   * The product’s title.
   */
  title: string;
  /**
   * Stripped description of the product, single line with HTML tags removed.
   */
  description: string;
  /**
   * List of the product’s variants.
   */
  variants: DetailsData_products_edges_node_variants;
  /**
   * The price range.
   */
  priceRange: DetailsData_products_edges_node_priceRange;
  /**
   * The date and time when the product was last modified.
   */
  updatedAt: ShopifyDateTime;
  /**
   * List of images associated with the product.
   */
  images: DetailsData_products_edges_node_images;
}

export interface DetailsData_products_edges {
  __typename: "ProductEdge";
  /**
   * The item at the end of ProductEdge.
   */
  node: DetailsData_products_edges_node;
}

export interface DetailsData_products {
  __typename: "ProductConnection";
  /**
   * A list of edges.
   */
  edges: DetailsData_products_edges[];
}

export interface DetailsData {
  /**
   * List of the shop’s products.
   */
  products: DetailsData_products;
}

export interface DetailsDataVariables {
  preferredContentType?: ImageContentType | null;
}
